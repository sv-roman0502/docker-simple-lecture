FROM python:3.9.9-slim-buster

# создать внутри докер образа пустую папку `src` и перейти в нее (сделать cd)
WORKDIR /src

COPY requirements.txt /src
RUN pip install -r requirements.txt

COPY ./src /src

RUN chmod +x ./entrypoint.sh

CMD ["./entrypoint.sh"]
