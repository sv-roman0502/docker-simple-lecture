from flask import Flask


app = Flask(__name__)


@app.route('/')
def index():
    return "Привет, Рома"


if __name__ == '__main__':
    app.run(host='0.0.0.0', port=5000, debug=True)


# docker run --rm -p 5001:5000 dockst:version1
# docker build -t dockst:version1 .
# docker run -p 5001:5000 dockst:version1
# namespace, cgroup
